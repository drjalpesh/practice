import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()
export class MyInterceptor implements HttpInterceptor {

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

        let MyJWT = localStorage.getItem('JWT');

        if (MyJWT) {
            request.headers.set('Authorization', `Bearer ${MyJWT}`);
        }

        return next.handle(request);
    }
}