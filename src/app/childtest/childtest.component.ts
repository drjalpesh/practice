import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-childtest',
  templateUrl: './childtest.component.html',
  styleUrls: ['./childtest.component.css']
})
export class ChildtestComponent implements OnInit {

  constructor() { }
  @Input()  txttest:string;
  @Output() outtest=new EventEmitter();

  ngOnInit() {
  }

  callbtn()
  {
this.outtest.emit(5);
  }
}
