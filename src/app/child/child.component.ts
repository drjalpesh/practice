import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { SharedService } from '../shared.service';

@Component({
  selector: 'app-child',
  templateUrl: './child.component.html',
  styleUrls: ['./child.component.css']
})
export class ChildComponent implements OnInit {

  @Input() Pdata: number;
  @Output() Odata = new EventEmitter();

  constructor(private sharedService: SharedService) { }

  ngOnInit() {


  }

  Export() {

    var temp = this.Pdata * this.Pdata;

    this.Odata.emit(temp);


    this.sharedService.TempData = "Hello from child ";
  }
}
