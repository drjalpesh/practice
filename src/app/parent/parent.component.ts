import { Component, OnInit, Input } from '@angular/core';
import { SharedService } from '../shared.service';

@Component({
  selector: 'app-parent',
  templateUrl: './parent.component.html',
  styleUrls: ['./parent.component.css']
})
export class ParentComponent implements OnInit {

  public value = 123;
  public Ovalue;

  constructor(private sharedService: SharedService) { }

  ngOnInit() {

    window.setInterval(() => {
      console.log(this.sharedService.TempData);
    }, 1000);

  }


  CalledFromChiled(event) {
    this.Ovalue = event;
  }
}
